"""
AI Video Script Generator

This script generates a sequence of images based on keyframes defined in a video script (YAML format) and stitches them together to create a video.

Parameters:
- fps (Frames Per Second): Controls the frame rate of the video. Default: 30. Range: 1-60. Higher values result in smoother motion.
- video_fps (Optional): Specifies the FPS for the video output, distinct from the `fps` used for frame generation. Default: Uses the same value as `fps` if not specified.
- seed: Sets the random seed for image generation. Default: 42. Any integer value. Different seeds produce different images for the same prompt.
- increment_seed_every_frame: If true, increments the seed for each frame, creating more variation. Default: False.
- feedback_loop: If true, uses the output of one frame as the input for the next. Default: False.
- guidance_scale: Influences the adherence to the prompt. Default: 7.5. Range: 1-20. Lower values generate more abstract images; higher values stick closer to the prompt.
- num_inference_steps: Number of steps for the model to refine the image. Default: 50. Range: 10-100. More steps can lead to more detailed images but take longer to generate.
- temperature: Controls the randomness. Default: 1.0. Range: 0.1-2.0. Lower values make the output more deterministic and conservative; higher values increase randomness and creativity.
- strength: Controls the influence of the input prompt. Default: 0.8. Range: 0-1. A lower value retains more of the original image (if using feedback_loop); a higher value gives more weight to the prompt.
- disjoint_keyframes: every new keyframe creates a new image from scratch, instead of using the last image from the last interpolation, only works if feedback_loop is set.
The script supports overriding these parameters for each keyframe in the video script, allowing for dynamic changes throughout the video.

"""

TEXT2IMAGE_GENERATION_MODEL = "runwayml/stable-diffusion-v1-5"
IMAGE2IMAGE_GENERATION_MODEL = "runwayml/stable-diffusion-v1-5"

'''
Image to image models:

"runwayml/stable-diffusion-v1-5"
"stabilityai/stable-diffusion-xl-refiner-1.0"
"kandinsky-community/kandinsky-2-2-decoder"
'''

import math
import argparse
import yaml
import os
import torch
import shutil
from diffusers import AutoPipelineForText2Image
from diffusers import AutoPipelineForImage2Image
import warnings
from PIL import Image
from common import check_ffmpeg_installed, calculate_total_duration, create_video, calculate_duration, timestamp_seconds, is_number, is_timestamp

warnings.filterwarnings("ignore", category=UserWarning, module="torch.utils._pytree")

def write_preview_info(directory, start_frame, end_frame, keyframe_index, keyframe_info):
    filename = os.path.join(directory, f"preview_{start_frame}-{end_frame}_keyframe_{keyframe_index}.txt")
    with open(filename, 'w') as file:
        file.write(f"Keyframe {keyframe_index} info:\n")
        for key, value in keyframe_info.items():
            file.write(f"{key}: {value}\n")

def write_debug_info(filename, prompt, seed, guidance_scale, num_inference_steps, temperature, strength, hadInputImage):
    with open(filename, 'w') as file:
        file.write(f"Prompt: {prompt}\n")
        file.write(f"Seed: {seed}\n")
        file.write(f"Guidance Scale: {guidance_scale}\n")
        file.write(f"Number of Inference Steps: {num_inference_steps}\n")
        file.write(f"Temperature: {temperature}\n")
        file.write(f"Strength: {strength}\n")
        file.write(f"Had input image: {hadInputImage}\n")

def pauseAndReloadLastFrame():
    input("Paused. Press Enter to continue...")
    if current_frame_count-1 >= 0:
        last_frame = os.path.join(args.output_dir, f"frame_{current_frame_count-1}.jpg")
        return Image.open(last_frame).convert("RGB")

def interpolate(start, end, factor, interpolation_type='linear'):
    if interpolation_type == 'linear':
        return start + (end - start) * factor
    elif interpolation_type == 'quadratic':
        return start + (end - start) * (factor ** 2)
    elif interpolation_type == 'quadratic_in':
        return start + (end - start) * (factor ** 2)
    elif interpolation_type == 'quadratic_out':
        return start + (end - start) * (1 - (1 - factor) ** 2)
    elif interpolation_type == 'exponential':
        return start + (end - start) * (2 ** (10 * (factor - 1)))
    elif interpolation_type == 'elastic':
        if factor == 0 or factor == 1:
            return start + (end - start) * factor
        return start + (end - start) * (2 ** (-10 * factor) * math.sin((factor - 0.075) * (2 * math.pi) / 0.3) + 1)
    elif interpolation_type == 'ease_in_out':
        return start + (end - start) * factor ** 2 * (3 - 2 * factor)
    elif interpolation_type == 'cubic_in':
        return start + (end - start) * factor ** 3
    elif interpolation_type == 'cubic_out':
        return start + (end - start) * (1 - (1 - factor) ** 3)
    elif interpolation_type == 'cubic_in_out':
        if factor < 0.5:
            return start + (end - start) * 4 * factor ** 3
        else:
            return start + (end - start) * (1 - pow(-2 * factor + 2, 3) / 2)
    else:
        raise ValueError(f"Unknown interpolation type: {interpolation_type}")

def generate_image(pipe, prompt, guidance_scale, num_inference_steps, temperature, strength, width=512, height=512):
    image = pipe(prompt=prompt,
                        guidance_scale=guidance_scale,
                        num_inference_steps=num_inference_steps,
                        temperature=temperature,
                        strength=strength,
                        width=width,
                        height=height).images[0]
    return image

def generate_from_image(pipe, prompt, guidance_scale, num_inference_steps, temperature, strength, previous_image, verbose, width=512, height=512):
    if verbose:
        print(f"Prompt: {prompt}, Guidance Scale: {guidance_scale}, Num Inference Steps: {num_inference_steps}, Temperature: {temperature}, Strength: {strength}, Previous Image: {'Yes' if previous_image else 'No'}")
    
    image = pipe(prompt=prompt,
                        guidance_scale=guidance_scale,
                        num_inference_steps=num_inference_steps,
                        temperature=temperature,
                        strength=strength,
                        image=previous_image,
                        width=width,
                        height=height).images[0]
    
    return image

def find_latest_frame(directory):
    frame_files = [f for f in os.listdir(directory) if f.startswith("frame_") and f.endswith(".jpg")]
    if not frame_files:
        return -1
    latest_frame = max(int(f.split('_')[1].split('.')[0]) for f in frame_files)
    return latest_frame

def find_latest_frame_and_keyframe(directory, keyframes, fps):
    frame_files = [f for f in os.listdir(directory) if f.startswith("frame_") and f.endswith(".jpg")]
    if not frame_files:
        return -1, 0, None
    latest_frame_number = max(int(f.split('_')[1].split('.')[0]) for f in frame_files)
    latest_frame_file = os.path.join(directory, f"frame_{latest_frame_number}.jpg")

    cumulative_frames = 0
    for i, keyframe in enumerate(keyframes):

        if i + 1 < len(keyframes):  # If there is a next keyframe
            next_keyframe = keyframes[i + 1]
        else:
            next_keyframe = None 

        keyframe_duration = calculate_duration(keyframe, next_keyframe)
        frames_in_keyframe = int(keyframe_duration * fps)

        cumulative_frames += frames_in_keyframe
        if cumulative_frames > latest_frame_number:
            return i, latest_frame_number - (cumulative_frames - frames_in_keyframe), latest_frame_file

    return len(keyframes) - 1, 0, latest_frame_file

def count_frames(keyframes, fps):
    total_frames = 0
    for i, keyframe in enumerate(keyframes):
        if i + 1 < len(keyframes):  # If there is a next keyframe
            next_keyframe = keyframes[i + 1]
        else:
            next_keyframe = None  # No next keyframe for the last one

        duration = calculate_duration(keyframe, next_keyframe)
        total_frames += int(duration * fps)
    return total_frames

parser = argparse.ArgumentParser(description="AI Video Script Generator")
parser.add_argument("script_file", nargs='?', default="video_script.yaml", help="Path to the video script YAML file.")
parser.add_argument("-cf", "--count-frames", action="store_true", help="Count the total frames in the video script without generating them.")
parser.add_argument("-od", "--output-dir", default="Output", help="Directory to save the generated images.")
parser.add_argument("-fo", "--fps-override", type=int, help="Override FPS specified in the video script.")
parser.add_argument("-vfo", "--video-fps-override", type=int, help="Override video FPS specified in the video script.")
parser.add_argument("-cv", "--create-video", action="store_true", help="Generate a video from the produced frames.")
parser.add_argument("-df", "--debug-frames", action="store_true", help="Create a debug file for each frame with used parameters.")
parser.add_argument("-d", "--debug", action="store_true", help="Create a single debug file for the entire run with used parameters.")
parser.add_argument("-vf", "--video-format", default="mp4", help="Format of the output video (e.g., mp4, avi).")
parser.add_argument("-vc", "--video-codec", default="libx264", help="Codec used for video encoding (e.g., libx264, libx265).")
parser.add_argument("-di", "--delete-images", action="store_true", help="Delete generated images after creating the video. CAUTION, NOT ADVISED.")
parser.add_argument("-iv", "--interpolate-video", action="store_true", help="Uses video interpolation if fps < video_fps.")
parser.add_argument("-bv", "--blend-video", action="store_true", help="Uses video blending if fps > video_fps.")
parser.add_argument("-v", "--verbose", action="store_true", help="Show debug information while working.")
parser.add_argument("-cg", "--continue-generation", action="store_true", help="Continue generation from the latest frame in the output directory.")
parser.add_argument("-t2im", "--t2i-model", type=str, help="Sets the text 2 image model.")
parser.add_argument("-i2im", "--i2i-model", type=str, help="Sets the image 2 image model.")
parser.add_argument("--preview", type=int, help="Limit frame generation to the specified number per keyframe for preview purposes.")

args = parser.parse_args()

if args.create_video:
    if not check_ffmpeg_installed():
        print("ffmpeg is not installed. Please install ffmpeg to create a video.")
        print("For Linux: sudo apt-get install ffmpeg")
        print("For macOS: brew install ffmpeg")
        exit(1)

with open(args.script_file, 'r') as file:
    video_script = yaml.safe_load(file)

# IMAGE MODELS
TEXT2IMAGE_GENERATION_MODEL = args.t2i_model or TEXT2IMAGE_GENERATION_MODEL
IMAGE2IMAGE_GENERATION_MODEL = args.i2i_model or IMAGE2IMAGE_GENERATION_MODEL

# FPS
fps = args.fps_override or video_script['fps']
video_fps = args.video_fps_override or video_script.get('video_fps', fps)

# Initialize a list to accumulate debug information if --debug is enabled
debug_info = []

if args.count_frames:
    print(f"Total frames: {count_frames(video_script['keyframes'], fps)}")
    exit(0)

# Image size
width = video_script.get('width', 512)
height = video_script.get('height', 512)
print(f"Generating images in the following size: w {width}x{height} h")

# TEXT TO IMAGE
t2i_pipeline = AutoPipelineForText2Image.from_pretrained(
    TEXT2IMAGE_GENERATION_MODEL, torch_dtype=torch.float16, variant="fp16", use_safetensors=True
)
t2i_pipeline.safety_checker = None
t2i_pipeline.requires_safety_checker = False
#pipeline.enable_model_cpu_offload()
t2i_pipeline.to("cuda")

# IMAGE TO IMAGE
i2i_pipeline = AutoPipelineForImage2Image.from_pretrained(
    IMAGE2IMAGE_GENERATION_MODEL, torch_dtype=torch.float16, variant="fp16", use_safetensors=True
)
i2i_pipeline.safety_checker = None
i2i_pipeline.requires_safety_checker = False
#i2i_pipeline.enable_model_cpu_offload()
i2i_pipeline.to("cuda")

os.makedirs(args.output_dir, exist_ok=True)

feedback_loop = video_script.get('feedback_loop', False)
disjoint_keyframes = video_script.get('disjoint_keyframes', False)

print(f'LOOP MODE: feedback loop {feedback_loop} disjoint keyframes {disjoint_keyframes}')

last_keyframe = video_script['keyframes'][-1]

if last_keyframe and 'duration' in last_keyframe and last_keyframe['duration'] > 0:
    new_keyframe = last_keyframe.copy()
    new_keyframe['duration'] = 0
    video_script['keyframes'].append(new_keyframe)

seed = video_script['seed']
increment_seed_every_frame = video_script.get('increment_seed_every_frame', False)
keyframes = video_script['keyframes']
total_frames = count_frames(keyframes, fps)
previous_image = None

# INITIAL IMAGE LOADING
initial_image_path = video_script.get('initial_image')
initial_image = None
if initial_image_path:
    initial_image = Image.open(os.path.dirname(args.script_file)+"/"+initial_image_path).convert("RGB")
    previous_image = initial_image

# GLOBAL PARAMETERS
global_guidance_scale = video_script.get('guidance_scale', 7.5)
global_num_inference_steps = video_script.get('num_inference_steps', 50)
global_temperature = video_script.get('temperature', 1.0)
global_strength = video_script.get('strength', 0.8) 
global_prompt_change = video_script.get('prompt_change', 1.0) 

# Setup Starting point
start_keyframe_index, start_subframe = 0, 0
current_frame_count = 0

# GENERATION CONTINUATION
if args.continue_generation:
    start_keyframe_index, start_subframe, latest_frame_file = find_latest_frame_and_keyframe(args.output_dir, keyframes, fps)
    # Calculate current_frame_count considering both duration and timestamp
    current_frame_count = start_subframe
    for i, kf in enumerate(keyframes[:start_keyframe_index]):
        if i + 1 < len(keyframes):  # If there is a next keyframe
            next_keyframe = keyframes[i + 1]
        else:
            next_keyframe = None  # No next keyframe for the last one in the slice
        current_frame_count += int(calculate_duration(kf, next_keyframe) * fps)
        
    initial_image = Image.open(latest_frame_file).convert("RGB")
    previous_image = initial_image
    print(f"Continuing generation from: {current_frame_count}. Start keyframe {start_keyframe_index}, start subframe {start_subframe}.")

for i, (current_keyframe, next_keyframe) in enumerate(zip(keyframes[start_keyframe_index:], keyframes[start_keyframe_index + 1:])):
    duration = calculate_duration(current_keyframe, next_keyframe)
    images_to_generate_for_keyframe = int(duration * fps)

    if args.preview:
        images_to_generate_for_keyframe = min(images_to_generate_for_keyframe, args.preview)
        print(f"preview mode. This amount of frames will be generated for the current keyframe: {images_to_generate_for_keyframe}")

    # Use global parameters or override if specified in keyframe
    guidance_scale = current_keyframe.get('guidance_scale', global_guidance_scale)
    num_inference_steps = current_keyframe.get('num_inference_steps', global_num_inference_steps)
    temperature = current_keyframe.get('temperature', global_temperature)
    strength = current_keyframe.get('strength', global_strength)

    pauseMode = None
    if 'pause' in current_keyframe:
        pauseMode = current_keyframe['pause']

    if pauseMode == 'before':
        previous_image = pauseAndReloadLastFrame()

    # First frame inference fix
    if current_frame_count == 0 and previous_image is None:
        num_inference_steps = global_num_inference_steps

    prompt_change = current_keyframe.get('prompt_change', global_prompt_change)

    for j in range(start_subframe if i == 0 else 0, images_to_generate_for_keyframe):
        if pauseMode == 'before_every_frame':
            previous_image = pauseAndReloadLastFrame()

        if current_frame_count == 1:
            num_inference_steps = current_keyframe.get('num_inference_steps', global_num_inference_steps)

        interpolation_type = current_keyframe.get('interpolation', video_script.get('interpolation', 'linear'))
        interpolation_factor = (j / images_to_generate_for_keyframe)

        # Interpolate parameters
        interpolated_guidance_scale = interpolate(guidance_scale, next_keyframe.get('guidance_scale', global_guidance_scale), interpolation_factor, interpolation_type)
        interpolated_num_inference_steps = int(interpolate(num_inference_steps, next_keyframe.get('num_inference_steps', global_num_inference_steps), interpolation_factor, interpolation_type))
        interpolated_temperature = interpolate(temperature, next_keyframe.get('temperature', global_temperature), interpolation_factor, interpolation_type)
        interpolated_strength = interpolate(strength, next_keyframe.get('strength', global_strength), interpolation_factor, interpolation_type)

        # Calculate current time - timestamp is required for timestamp pausemode
        if('timestamp' in current_keyframe):
            current_time_stamp_seconds = timestamp_seconds(current_keyframe['timestamp'])
            current_keyframe_elapsed_seconds = duration*interpolation_factor
            current_total_time_seconds = current_time_stamp_seconds + current_keyframe_elapsed_seconds

            # Pause if pause mode is a duration within the frame
            if is_number(pauseMode) and current_keyframe_elapsed_seconds > float(pauseMode):
                previous_image = pauseAndReloadLastFrame()
            
            if is_timestamp(pauseMode) and current_total_time_seconds > timestamp_seconds(pauseMode):
                previous_image = pauseAndReloadLastFrame()

        # Frame file name
        filename = os.path.join(args.output_dir, f"frame_{current_frame_count}.jpg")

        has_previous = previous_image is not None

        if interpolation_factor > prompt_change:
            current_prompt = next_keyframe['prompt']
        else:
            current_prompt = current_keyframe['prompt']

        torch.manual_seed(seed)

        # Generate the image with the interpolated parameters
        new_image = None
        if feedback_loop and previous_image is not None:
            new_image = generate_from_image(
                i2i_pipeline, current_prompt, 
                interpolated_guidance_scale, interpolated_num_inference_steps,
                interpolated_temperature, interpolated_strength, previous_image,
                args.verbose, width, height)
        else:
            new_image = generate_image(
                t2i_pipeline, current_prompt,
                interpolated_guidance_scale, interpolated_num_inference_steps, 
                interpolated_temperature, interpolated_strength, width, height)

        new_image.save(filename)
        previous_image = new_image

        if args.debug_frames:
            debug_filename = os.path.join(args.output_dir, f"debug_frame_{current_frame_count}_keyframe{i}_subframe{j}.txt")
            write_debug_info(debug_filename, current_keyframe['prompt'], seed, interpolated_guidance_scale, interpolated_num_inference_steps, interpolated_temperature, interpolated_strength, has_previous)

        if args.debug:
            debug_info.append(f"Frame {current_frame_count}: Prompt: {current_prompt}, Seed: {seed}, Guidance Scale: {interpolated_guidance_scale}, Num Inference Steps: {interpolated_num_inference_steps}, Temperature: {interpolated_temperature}, Strength: {interpolated_strength}, Had input image: {has_previous}")

        current_frame_count += 1
        percent_done = (current_frame_count / total_frames) * 100
        if args.verbose:
            print(f"{percent_done:.2f}% frame {current_frame_count}/{total_frames}")

        if increment_seed_every_frame:
            seed += 1

        if pauseMode == 'after_every_frame':
            previous_image = pauseAndReloadLastFrame()
            
    if args.preview:
            write_preview_info(args.output_dir, current_frame_count - images_to_generate_for_keyframe, current_frame_count - 1, i, current_keyframe)
            print(f"preview mode: next keyframe.")

    if pauseMode == 'after':
        previous_image = pauseAndReloadLastFrame()

    pauseMode = None

    if disjoint_keyframes or args.preview:
        previous_image = None

    if i == 0:
        start_subframe = 0

if args.create_video:
    total_duration = calculate_total_duration(video_script['keyframes'])
    video_file = f"{args.script_file.removesuffix('.yaml')}_video"
    create_video(args.output_dir, video_file, fps, video_fps, total_duration, args.video_format, args.video_codec, args.interpolate_video, args.blend_video)
    print(f"Video created: {video_file}.{args.video_format}")

    if args.delete_images:
        print("Deleting generated images...")
        shutil.rmtree(args.output_dir)
        print("Images deleted.")

if args.debug:
    debug_filename = f"{args.script_file.removesuffix('.yaml')}_generation_debug.txt"
    with open(debug_filename, 'w') as file:
        file.write("\n".join(debug_info))

    print(f"Comprehensive debug information written to {debug_filename}")