# Zombie Dreams - AI Video Script Generator

This Python script leverages AI to generate a sequence of images based on keyframes defined in a YAML format video script. It stitches these images together to create a video, offering a unique blend of visual storytelling powered by AI.

## Features

 * Dynamic Video Creation: Generate videos from textual descriptions with customizable frame rates, styles, and dynamics.
 * Keyframe-Based Scripting: Define keyframes in YAML format to control the video narrative, appearance, and transitions.
 * Advanced Image Generation: Utilize state-of-the-art models for text-to-image and image-to-image generation.
 * Customizable Parameters: Tailor the video generation process with parameters like frame rate, seed, feedback loop, interpolation, and more.
 * Interactive Editing: Add pauses, edit images manually to insert text or seed for the AI to eat.
 * Safe Generation: If you lose power during generation, it can resume the current generation from where it left of.

## Getting Started

### Prerequisites

Run: `pip install torch torchvision torchaudio --index-url https://download.pytorch.org/whl/cu118`

 * Python 3.6+
 * PyTorch
 * diffusers
 * PIL
 * ffmpeg (for video creation) `sudo apt install ffmpeg`

## Installation

1. Clone this repository.
2. Install the required dependencies: `pip install -r requirements.txt` (I'm hoping this works? Send tickets if it doesn't)

## Usage

`python VideoGen.py <filename> [script arguments]`

### Full example

Generate the video for the gpt3psych script. Uses the `temp` directory to create the images while making the video.
Deletes the image dir after. Prints verbose updates.

`python VideoGen.py videoScripts/gpt3psych.yaml -od Temp --create-video -di -v`

### Program Arguments

| Shortcuts | Argument                     | Description |
| --------- | ---------------------------- | ----------- |
|           | `<filename>`                 | Path to the video script YAML file. Default: `video_script.yaml`. |
| -cf       | --count-frames               | Count the total frames in the video script without generating them. |
| -od       | --output-dir `<directory>`   | Directory to save the generated images. Default: `Output`. |
| -fo       | --fps-override `<fps>`       | Override FPS specified in the script. |
| -vfo      | --video-fps-override `<fps>` | Override video FPS specified in the video script. EXPERIMENTAL. |
| -cv       | --create-video               | Generate a video from the produced frames. |
| -df       | --debug-frames               | Create a debug file for each frame with used parameters. |
| -d        | --debug                      | Create a single debug file for the entire run with used parameters. |
| -vf       | --video-format `<format>`    | Format of the output video (e.g., mp4, avi). Default: `mp4`. |
| -vc       | --video-codec `<codec>`      | Codec used for video encoding (e.g., libx264, libx265). Default: `libx264`. |
| -di       | --delete-images              | Delete generated images after creating the video. CAUTION, NOT ADVISED. |
| -iv       | --interpolate-video          | Uses video interpolation if fps < video_fps. EXPERIMENTAL. |
| -bv       | --blend-video                | Uses video blending if fps > video_fps. EXPERIMENTAL. |
| -v        | --verbose                    | Show debug information while working. |
| -cg       | --continue-generation        | Continue generation from the latest frame in the output directory. Only use if you have already generated something. |
|-t2im      | --t2i-model                  | Sets the text to image model. Default: `runwayml/stable-diffusion-v1-5`|
|-i2im      | --i2i-model                  | Sets the image to image model. Default: `runwayml/stable-diffusion-v1-5` |
|           | --preview `<frames>`         | Limit frame generation to the specified number per keyframe for preview purposes. Keyframe generation disjunction forced. |

### Tested image models

 * runwayml/stable-diffusion-v1-5
 * stabilityai/stable-diffusion-xl-refiner-1.0
 * kandinsky-community/kandinsky-2-2-decoder

## Video Scripts

### Example Video Script

```yaml
fps: 30
seed: 65498
increment_seed_every_frame: true
feedback_loop: true
guidance_scale: 17
num_inference_steps: 60
temperature: 0.7
strength: 0.5
interpolation: quadratic

keyframes:
  - prompt: "a boat on a river, painting. water flowing with blue thick stripes. pastel. blue colors."
    duration: 5
    temperature: 0.5
    strength: 0.05
    num_inference_steps: 180
    interpolation: cubic_out
  - prompt: "a boat on a river with emerging tangerine trees on the bank, watercolor painting with blue and orange hues."
    duration: 5
    temperature: 0.8
    strength: 1.0
    num_inference_steps: 60
    interpolation: cubic_in
  - prompt: "river scene transitioning to tangerine trees, pastel painting with blue and orange hues."
    duration: 3.5
    temperature: 0.2
    strength: 0.3
    num_inference_steps: 70
  - prompt: "tangerine trees with marmalade skies in the background, vibrant pastel hues."
    duration: 3.5
    temperature: 0.35
    strength: 0.5
  - prompt: "tangerine trees and marmalade skies. abstract, painting."
    duration: 3.5
    temperature: 0.9
    strength: 0.8
    num_inference_steps: 80
    interpolation: cubic_in_out
```

### GPT Agent

I have created a GPT agent to write these video scripts: [Zombie Dreams Writer](https://chat.openai.com/g/g-oDrZV0oly-zombie-dreams-script-writer)

## Tips for Effective Script Writing

### Useful things

If you are doing feedback_loop, dont start with 1 strength and have a second keyframe with also 1 strength. This causes the first frame to change too much on the first loop that it becomes a frame that has nothing to do with the video. If you want more stability for that, change the seed every frame, this will prevent fast noise accumulation without generating anything.

### Strength and Inference Steps

It seems that setting a lower strength causes the feedback loop to create new images based on prior images running fewer inference steps. For example,
if you set your first keyframe to run at 80 inference steps, and you have the feedback loop, the frames generated after that will run with 80*strenght steps.
This is so the image doesn't change much because a lower strength is an image more similar to the input.

I like to increase the inference steps when I'm lowering the strength so it still creates nice images without just adding noise.

### Nice transitions that I have found

```yaml
- prompt: "the mushrooms start to float, creating a spiral above the forest"
    duration: 5
    num_inference_steps: 60
    temperature: 0.4
    guidance_scale: 12
    strength: 0.5
- prompt: "a portal opens in the sky, revealing a cosmic space with nebulas"
    duration: 10
    temperature: 1.0
    guidance_scale: 14
    strength: 0.7
```

```yaml
 - prompt: "a portal opens in the sky, revealing a cosmic space with nebulas"
    duration: 10
    temperature: 1.0
    guidance_scale: 14
    strength: 0.7
 - prompt: "the view transitions into a surreal cityscape, with buildings defying gravity"
    duration: 5
    temperature: 0.7
    guidance_scale: 16
    strength: 0.6
```

## Script Arguments

| Argument                    | Description                                                                                                                               | Default | Range                               | Place |
|-----------------------------|-------------------------------------------------------------------------------------------------------------------------------------------|---------|-------------------------------------|-------------|
| fps                         | Controls the frame rate of the video. Higher values result in smoother motion.                                                            | 30      | 1+                                | Header           |
| video_fps (Optional)        | Specifies the FPS for the video output, distinct from the `fps` used for frame generation. Allows for creative control over playback speed. | Same as `fps` | -                               | Header           |
| seed                        | Sets the random seed for image generation. Different seeds produce different images for the same prompt.                                  | 42      | Any integer                         | Header           |
| increment_seed_every_frame  | If true, increments the seed for each frame, creating more variation.                                                                     | False   | True, False                                   | Header           |
| feedback_loop               | If true, uses the output of one frame as the input for the next.                                                                          | False   | True, False                                   | Header           |
| guidance_scale              | Influences the adherence to the prompt. Lower values generate more abstract images; higher values stick closer to the prompt.             | 7.5     | 1-20                                | Header, Keyframes           |
| num_inference_steps         | Number of steps for the model to refine the image. More steps can lead to more detailed images but take longer to generate.               | 50      | 10-100                              | Header, Keyframes           |
| temperature                 | Controls the randomness. Lower values make the output more deterministic and conservative; higher values increase randomness and creativity. | 1.0     | 0.1-2.0                             | Header, Keyframes           |
| strength                    | Controls the influence of the input prompt. A lower value retains more of the original image; a higher value gives more weight to the prompt. | 0.8     | 0-1                                 | Header, Keyframes           |
| interpolation (Optional)    | Sets the type of interpolation for keyframe transitions.                                                                                  | linear  | linear, quadratic, quadratic_in, quadratic_out, exponential, elastic, ease_in_out, cubic_in, cubic_out, cubic_in_out | Header, Keyframes           |
| pause (Optional)            | Pauses the generation for interactive editing. Before pauses before the keyframe starts generating. After pauses before the next keyframe starts. If pause is set as a duration, then it's interpreted to pause after that amount of seconds passed in the current keyframe. If it's a timestamp between the current keyframe timestamp and the next, it pauses there. These 2 only work if the script is in timestamp mode. | Not set | "before", "after", duration, timestamp  | Keyframes           |
| prompt_change (Optional) | Defines at which point between 2 keyframes, the prompt is changed from the current keyframe to the next. The default is 1.0, which means the prompt only changes when the next keyframe starts being generated. A 0.5 prompt change makes the promt change in the middle of the keyframe generation. | 1.0 | "before", "after" | Header, Keyframes           |
| duration               | The number of seconds of video to generate for this keyframe. Can't be used along with timestamps. | -   | any decimal representing seconds. | Keyframes           |
| timestamp              | The timestamp where this keyframe starts playing. Can't be used if duration is beign used. | -   | one of: hh:mm:ss.hs or mm:ss.s | Keyframes           |

## FPS Adjustments and Video Output

When using the `video_fps` argument in your video script, you can specify a different frame rate for the video output compared to the frame generation rate. This allows for creative flexibility in how your video is presented.

### Why Adjust Video FPS?

Adjusting the video FPS allows you to:

- Generate frames at a high rate (e.g., 60fps) for detailed motion sampling, then output at a lower rate (e.g., 30fps) to achieve a slow-motion effect without losing frame detail.
- Create a smoother video by interpolating frames at a higher fps, enhancing motion fluidity in your final video.

### Effects of Adjusting Video FPS

- **Lower Video FPS**: Slows down playback, extending the video's duration for a slow-motion effect. All generated frames are used, each displayed longer.
- **Higher Video FPS**: Speeds up playback, reducing the video's duration. This can create a fast-forward effect without adding or skipping frames.

## Image Generation Parameters

### Strength

strength is one of the most important parameters to consider and it'll have a huge impact on your generated image. It determines how much the generated image resembles
 the initial image. In other words:

 * a higher strength value gives the model more “creativity” to generate an image that's different from the initial image; a strength value of 1.0 means the initial image is more or less ignored
 * a lower strength value means the generated image is more similar to the initial image

The strength and num_inference_steps parameters are related because strength determines the number of noise steps to add. 
For example, if the num_inference_steps is 50 and strength is 0.8, then this means adding 40 (50 * 0.8) steps of noise to the initial image and 
then denoising for 40 steps to get the newly generated image.

### Guidance Scale

The guidance_scale parameter is used to control how closely aligned the generated image and text prompt are.
A higher guidance_scale value means your generated image is more aligned with the prompt, 
while a lower guidance_scale value means your generated image has more space to deviate from the prompt.

You can combine guidance_scale with strength for even more precise control over how expressive the model is. 
For example, combine a high strength + guidance_scale for maximum creativity or use a combination of low 
strength and low guidance_scale to generate an image that resembles the initial image but is not as strictly 
bound to the prompt.

## Image to image models

from: https://huggingface.co/docs/diffusers/using-diffusers/img2img

### Stable Diffusion v1.5

"runwayml/stable-diffusion-v1-5"

Stable Diffusion v1.5 is a latent diffusion model initialized from an earlier checkpoint, and further finetuned for 595K steps on 512x512 images. To use this pipeline for image-to-image, you’ll need to prepare an initial image to pass to the pipeline. Then you can pass a prompt and the image to the pipeline to generate a new image.

### Stable Diffusion XL (SDXL)

"stabilityai/stable-diffusion-xl-refiner-1.0"

SDXL is a more powerful version of the Stable Diffusion model. It uses a larger base model, and an additional refiner model to increase the quality of the base model's output. 
Read the SDXL guide for a more detailed walkthrough of how to use this model, and other techniques it uses to produce high quality images.

### Kandinsky 2.2

"kandinsky-community/kandinsky-2-2-decoder"

The Kandinsky model is different from the Stable Diffusion models because it uses an image prior model to create image embeddings.
The embeddings help create a better alignment between text and images, allowing the latent diffusion model to generate better images.

## To-Do

 * [ ] Add negative prompt support.
 * [ ] When we advance the seed, we need to either save or calculate the seed when resuming a generation.
 * [ ] Trying to be deterministc and resume with just the frames might work but its better if we actually update a file every frame, with 
  * The current seed
  * The current keyframe and subframe within that

## Good Video Scripts

There are some good examples on the videoScripts directory. Grab one of them, have fun! Change the seed, add keyframes...
The scripts that I have edited the most are: lucy.yaml, gpt_dreams_lucy_moving_seed.yaml, gpt3psych2.yaml

### Add audio to a video

ffmpeg -i video.avi -i audio.mp3 -codec copy -shortest output.avi

Here's what each part does:

    -i video.avi: This is your input video file.
    -i audio.mp3: This is the MP3 file you want to add as the audio track.
    -codec copy: This tells FFmpeg to copy the video and audio streams without re-encoding them, keeping the original quality intact.
    -shortest: This option makes the output file as long as the shortest input file. Useful if you don't want the video to drag on if the audio is longer.
    output.avi: This is your output file with the video and new audio track.

## Anxious people - Preview Generation

Run this while the video is being made.

Running on the output directory:
`ffmpeg -r 30 -i frame_%d.jpg -c:v libx264 -vf fps=30 -pix_fmt yuv420p ../preview.mp4`

Running on the program directory:
`ffmpeg -r 30 -i Output/frame_%d.jpg -c:v libx264 -vf fps=30 -pix_fmt yuv420p preview.mp4`

## Generate video from frames - MakeVideo.py

There's a make video python (MakeVideo.py) script that allows you to render a video from a folder using the video ai script.
To use it, you call it passing the videoScript and similar settings (same output directory and so on).

`python3 MakeVideo.py videoScripts/theEgg.yaml`

### Supported Arguments for Make Video

| Shortcuts | Argument                     | Description |
| --------- | ---------------------------- | ----------- |
|           | `<script_file>`              | Path to the video script YAML file. Default: `video_script.yaml`. |
| -od       | --output-dir `<directory>`   | Directory to save the generated images. Default: `Output`. |
| -fo       | --fps-override `<fps>`       | Override FPS specified in the script. |
| -vfo      | --video-fps-override `<fps>` | Override video FPS specified in the video script. EXPERIMENTAL |
| -vf       | --video-format `<format>`    | Format of the output video (e.g., mp4, avi). Default: `mp4`. |
| -vc       | --video-codec `<codec>`      | Codec used for video encoding (e.g., libx264, libx265). Default: `libx264`. |
| -iv       | --interpolate-video          | Uses video interpolation if fps < video_fps. EXPERIMENTAL |
| -bv       | --blend-video                | Uses video blending if fps > video_fps. EXPERIMENTAL |