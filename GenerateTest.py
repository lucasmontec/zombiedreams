from diffusers import DiffusionPipeline
import torch

pipeline = DiffusionPipeline.from_pretrained("runwayml/stable-diffusion-v1-5", torch_dtype=torch.float16)
pipeline.safety_checker = None
pipeline.requires_safety_checker = False
pipeline.to("cuda")

# Parameters for tuning
seed = 42  # Seed for reproducibility
guidance_scale = 7.5  # Guidance scale (usual: 7.5, min: ~0, max: ~20)
num_inference_steps = 50  # Diffusion steps (usual: 50, min: ~10, max: ~100)
temperature = 1.0  # Temperature (usual: 1.0, min: ~0.1, max: ~2.0)

# Apply the seed
torch.manual_seed(seed)

# Generate the image with the set parameters
image = pipeline(prompt="An image of a squirrel in Picasso style",
                 guidance_scale=guidance_scale,
                 num_inference_steps=num_inference_steps,
                 temperature=temperature).images[0]

# Save the image
image.save("output.jpg")
