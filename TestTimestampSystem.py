from common import calculate_duration, timestamp_seconds

print(timestamp_seconds("00:05:25.08"))
print(timestamp_seconds("00:06:23.08"))

keyframe = {
    "timestamp" : "00:05:25.08"
}

next_keyframe = {
    "timestamp" : "00:06:23.08"
}

print(calculate_duration(keyframe, next_keyframe))