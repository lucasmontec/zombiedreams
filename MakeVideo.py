# If your generation fails, you can still make a video 

import argparse
import yaml

from common import check_ffmpeg_installed, calculate_total_duration, create_video

parser = argparse.ArgumentParser(description="AI Video Script Generator")
parser.add_argument("script_file", nargs='?', default="video_script.yaml", help="Path to the video script YAML file")
parser.add_argument("-od", "--output-dir", default="Output", help="Directory to save the generated images")
parser.add_argument("-vf", "--video-format", default="mp4", help="Format of the output video (e.g., mp4, avi)")
parser.add_argument("-vc", "--video-codec", default="libx264", help="Codec used for video encoding (e.g., libx264, libx265)")
parser.add_argument("-iv", "--interpolate-video", action="store_true", help="Uses video interpolation if fps < video_fps")
parser.add_argument("-bv", "--blend-video", action="store_true", help="Uses video blending if fps > video_fps")
parser.add_argument("-fo", "--fps-override", type=int, help="Override FPS specified in the video script")
parser.add_argument("-vfo", "--video-fps-override", type=int, help="Override video FPS specified in the video script")

args = parser.parse_args()

with open(args.script_file, 'r') as file:
    video_script = yaml.safe_load(file)

fps = args.fps_override or video_script['fps']
video_fps = args.video_fps_override or video_script.get('video_fps', fps)

if not check_ffmpeg_installed():
    print("ffmpeg is not installed. Please install ffmpeg to create a video.")
    print("For Linux: sudo apt-get install ffmpeg")
    print("For macOS: brew install ffmpeg")
    exit(1)

total_duration = calculate_total_duration(video_script['keyframes'])
video_file = f"{args.script_file.removesuffix('.yaml')}_video"
create_video(args.output_dir, video_file, fps, video_fps, total_duration, args.video_format, args.video_codec, args.interpolate_video, args.blend_video)