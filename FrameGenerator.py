import argparse
import yaml
import os
import torch
import subprocess
import shutil
from diffusers import DiffusionPipeline
import warnings

warnings.filterwarnings("ignore", category=UserWarning, module="torch.utils._pytree")

def write_debug_info(filename, prompt, seed, guidance_scale, num_inference_steps, temperature):
    with open(filename, 'w') as file:
        file.write(f"Prompt: {prompt}\n")
        file.write(f"Seed: {seed}\n")
        file.write(f"Guidance Scale: {guidance_scale}\n")
        file.write(f"Number of Inference Steps: {num_inference_steps}\n")
        file.write(f"Temperature: {temperature}\n")

def check_ffmpeg_installed():
    return shutil.which("ffmpeg") is not None

def create_video(input_dir, output_file, fps, video_format='mp4', codec='libx264'):
    command = [
        'ffmpeg',
        '-r', str(fps),
        '-i', os.path.join(input_dir, 'frame_%d.jpg'),
        '-c:v', codec,
        '-vf', 'fps=' + str(fps),
        '-pix_fmt', 'yuv420p',
        output_file + '.' + video_format
    ]
    subprocess.run(command)

def interpolate(start, end, num_frames, current_frame):
    return start + (end - start) * (current_frame / num_frames)

def generate_image(pipeline, filename, prompt, seed, guidance_scale, num_inference_steps, temperature):
    torch.manual_seed(seed)
    image = pipeline(prompt=prompt,
                     guidance_scale=guidance_scale,
                     num_inference_steps=num_inference_steps,
                     temperature=temperature).images[0]
    image.save(filename)

def count_frames(keyframes, fps):
    return sum(int(frame['delay'] * fps) for frame in keyframes)

parser = argparse.ArgumentParser(description="AI Video Script Generator")
parser.add_argument("script_file", nargs='?', default="video_script.yaml", help="Path to the video script YAML file")
parser.add_argument("--count-frames", action="store_true", help="Count the total frames in the video script without generating them")
parser.add_argument("--output-dir", default="Output", help="Directory to save the generated images")
parser.add_argument("--fps-override", type=int, help="Override FPS specified in the script")
parser.add_argument("--create-video", action="store_true", help="Generate a video from the produced frames")
parser.add_argument("--debug", action="store_true", help="Create a debug file for each frame with used parameters")
parser.add_argument("--video-format", default="mp4", help="Format of the output video (e.g., mp4, avi)")
parser.add_argument("--video-codec", default="libx264", help="Codec used for video encoding (e.g., libx264, libx265)")
parser.add_argument("--delete-images", action="store_true", help="Delete generated images after creating the video")
args = parser.parse_args()

if args.create_video:
    if not check_ffmpeg_installed():
        print("ffmpeg is not installed. Please install ffmpeg to create a video.")
        print("For Linux: sudo apt-get install ffmpeg")
        print("For macOS: brew install ffmpeg")
        exit(1)

with open(args.script_file, 'r') as file:
    video_script = yaml.safe_load(file)

fps = args.fps_override or video_script['fps']

if args.count_frames:
    print(f"Total frames: {count_frames(video_script['keyframes'], fps)}")
    exit(0)

pipeline = DiffusionPipeline.from_pretrained("runwayml/stable-diffusion-v1-5", torch_dtype=torch.float16)
pipeline.safety_checker = None
pipeline.requires_safety_checker = False
pipeline.to("cuda")

os.makedirs(args.output_dir, exist_ok=True)

feedback_loop = False
if 'feedback_loop' in video_script:
    feedback_loop = video_script['feedback_loop']

last_keyframe = video_script['keyframes'][-1]

if last_keyframe and 'delay' in last_keyframe and last_keyframe['delay'] > 0:
    # Create a new keyframe as a copy of the last keyframe with delay set to 0
    new_keyframe = last_keyframe.copy()
    new_keyframe['delay'] = 0
    video_script['keyframes'].append(new_keyframe)

seed = video_script['seed']
increment_seed_every_frame = video_script.get('increment_seed_every_frame', False)
keyframes = video_script['keyframes']
total_frames = count_frames(keyframes, fps)
current_frame_count = 0
previous_image = None

for i, (current_frame, next_frame) in enumerate(zip(keyframes, keyframes[1:])):
    num_images = int(current_frame['delay'] * fps)
    for j in range(num_images):
        # Interpolate parameters
        interpolated_guidance_scale = interpolate(current_frame['guidance_scale'], next_frame['guidance_scale'], num_images, j)
        interpolated_num_inference_steps = int(interpolate(current_frame['num_inference_steps'], next_frame['num_inference_steps'], num_images, j))
        interpolated_temperature = interpolate(current_frame['temperature'], next_frame['temperature'], num_images, j)

        # Frame file name
        filename = os.path.join(args.output_dir, f"frame_{current_frame_count}.jpg")

        # Generate the image with the interpolated parameters
        if feedback_loop and previous_image is not None:
            generate_image(pipeline, filename, current_frame['prompt'], seed, interpolated_guidance_scale, interpolated_num_inference_steps, interpolated_temperature, previous_image)
        else:
            generate_image(pipeline, filename, current_frame['prompt'], seed, interpolated_guidance_scale, interpolated_num_inference_steps, interpolated_temperature)

        # Debug information
        if args.debug:
            debug_filename = os.path.join(args.output_dir, f"debug_frame_{current_frame_count}_keyframe{i}_subframe{j}.txt")
            write_debug_info(debug_filename, current_frame['prompt'], seed, interpolated_guidance_scale, interpolated_num_inference_steps, interpolated_temperature)

        current_frame_count = i * num_images + j
        percent_done = (current_frame_count / total_frames) * 100
        print(f"{percent_done:.2f}% frame {current_frame_count}/{total_frames}")

        if increment_seed_every_frame:
            seed += 1

if args.create_video:
    video_file = f"{args.script_file}_video"
    create_video(args.output_dir, video_file, fps, args.video_format, args.video_codec)
    print(f"Video created: {video_file}.{args.video_format}")

    if args.delete_images:
        print("Deleting generated images...")
        shutil.rmtree(args.output_dir)
        print("Images deleted.")