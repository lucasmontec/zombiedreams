import shutil
import os
import subprocess

def calculate_total_duration(keyframes):
    total_duration = 0
    for i, frame in enumerate(keyframes):
        if 'duration' in frame:
            total_duration += frame['duration']
        elif 'timestamp' in frame:
            if i + 1 < len(keyframes):
                next_frame = keyframes[i + 1]
                total_duration += calculate_duration(frame, next_frame)
            else:
                total_duration += frame.get('duration', 0)
    return total_duration

def check_ffmpeg_installed():
    return shutil.which("ffmpeg") is not None

def create_video(input_dir, output_file, fps, video_fps, total_duration, video_format='mp4', codec='libx264', interpolate=False, blend=False):
    video_fps = video_fps or fps
    total_frames = int(total_duration * video_fps)

    command = [
        'ffmpeg',
        '-r', str(fps),  # Input FPS
        '-i', os.path.join(input_dir, 'frame_%d.jpg'),
        '-c:v', codec,
        '-t', str(total_duration),  # Total duration of the video
        '-vf', f'fps={video_fps}',  # Output FPS
    ]

    if interpolate and fps < video_fps:
        command += ['-vf', f"minterpolate='fps={video_fps}'"]
    elif blend and fps > video_fps:
        command += ['-vf', 'tblend=average,framestep=2', '-r', str(video_fps)]

    command += ['-pix_fmt', 'yuv420p', '-frames:v', str(total_frames), output_file + '.' + video_format]
    subprocess.run(command)

def is_number(value):
    if value is None:
        return False
    return value.replace('.','',1).isdigit()

def is_timestamp(value):
    if value is None:
        return False
    return ":" in value and value.replace(':','').isdigit()

def timestamp_seconds(timestamp):
    parts = [float(part) for part in timestamp.split(":")]
    if len(parts) == 3:  # hours:minutes:seconds
        total_seconds = parts[0] * 3600 + parts[1] * 60 + parts[2]
    elif len(parts) == 2:  # minutes:seconds
        total_seconds = parts[0] * 60 + parts[1]
    else:  # just seconds
        total_seconds = parts[0]
    return total_seconds

def calculate_duration(keyframe, next_keyframe):
    if 'timestamp' not in keyframe:
        return keyframe.get('duration', 0)

    start_seconds = timestamp_seconds(keyframe['timestamp'])
    if next_keyframe is not None:
        end_seconds = timestamp_seconds(next_keyframe['timestamp'])
        duration = end_seconds - start_seconds

        if(duration < 0):
            print(f"Next keyframe time {next_keyframe['timestamp']} - {end_seconds}s is before current frame {keyframe['timestamp']} - {start_seconds}!") 
            duration = 0 
    else:
        duration = keyframe.get('duration', 0)

    if duration == 0:
        print("ZERO DURATION. Are timestamps wrong? Are you missing a duration attribute?")

    return duration
