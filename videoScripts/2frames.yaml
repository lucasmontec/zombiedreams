#- fps (Frames Per Second): Controls the frame rate of the video. Default: 30. Range: 1-60. Higher values result in smoother motion.
#- seed: Sets the random seed for image generation. Default: 42. Any integer value. Different seeds produce different images for the same prompt.
#- increment_seed_every_frame: If true, increments the seed for each frame, creating more variation. Default: False.
#- feedback_loop: If true, uses the output of one frame as the input for the next. Default: False.
#- guidance_scale: Influences the adherence to the prompt. Default: 7.5. Range: 1-20. Lower values generate more abstract images; higher values stick closer to the prompt.
#- num_inference_steps: Number of steps for the model to refine the image. Default: 50. Range: 10-100. More steps can lead to more detailed images but take longer to generate.
#- temperature: Controls the randomness. Default: 1.0. Range: 0.1-2.0. Lower values make the output more deterministic and conservative; higher values increase randomness and creativity.
#- strength: Controls the influence of the input prompt. Default: 0.8. Range: 0-1. A lower value retains more of the original image (if using feedback_loop); a higher value gives more weight to the prompt.

# This example shows how to configure a script to create 2 almost identical frames.
# the feedback loop must be enabled to use the images in a chain, and the strength must be set to a low value.

# to run: python3 VideoGen.py 2frames.yaml
# to make a video: python3 VideoGen.py 2frames.yaml --create-video --delete-images

fps: 2
seed: 42
increment_seed_every_frame: false  # Set to true to increment seed for each frame
feedback_loop: true
guidance_scale: 15
num_inference_steps: 50
temperature: 0.4
strength: 0.1
keyframes:
  - prompt: "an egg"
    duration: 0.5
  - prompt: "a green egg"
    duration: 0.5